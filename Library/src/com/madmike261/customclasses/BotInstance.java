package com.madmike261.customclasses;

import java.util.ArrayList;
import java.util.List;

public class BotInstance {
	private String botName;
	private String userName;
	private String publicKey;
	private String privateKey;
	private String exchange;
	private double exchangeFee = 0.5;
	private double offsetPercentage = 0.2;
	private double profitPercentage = 0.28;
	private double takeProfitPercentage = 100;
	private int maxSlotCount = 1;
	private List<TradePair> tradePairs = new ArrayList<>();
	
	public String getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public double getExchangeFee() {
		return exchangeFee;
	}

	public void setExchangeFee(double exchangeFee) {
		this.exchangeFee = exchangeFee;
	}
	
	public List<TradePair> getTradePairs() {
		return tradePairs;
	}

	public void setTradePairs(List<TradePair> tradePairs) {
		this.tradePairs = tradePairs;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public double getOffsetPercentage() {
		return offsetPercentage;
	}

	public void setOffsetPercentage(double offsetPercentage) {
		this.offsetPercentage = offsetPercentage;
	}

	public double getProfitPercentage() {
		return profitPercentage;
	}

	public void setProfitPercentage(double profitPercentage) {
		this.profitPercentage = profitPercentage;
	}
	
	public double getTakeProfitPercentage() {
		return takeProfitPercentage;
	}

	public void setTakeProfitPercentage(double takeProfitPercentage) {
		this.takeProfitPercentage = takeProfitPercentage;
	}

	public int getMaxSlotCount() {
		return maxSlotCount;
	}

	public void setMaxSlotCount(int maxSlotCount) {
		this.maxSlotCount = maxSlotCount;
	}
}