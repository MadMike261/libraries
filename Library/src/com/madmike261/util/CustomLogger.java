package com.madmike261.util;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class CustomLogger {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");

	/**
	 * Initialize the Logger
	 * 
	 * @param String logDir: The Path of the LogDirectory
	 * @param Level  logLevel: The desired LogLevel
	 */
	public static void init(String logDir, Level logLevel) {
		// Create logging directories if needed
		String logFile = initLogDirs(logDir);

		try {
			// Assigning handlers to LOGGER object
			FileHandler fileHandler = new FileHandler(logFile, true);
			fileHandler.setFormatter(new SimpleFormatter() {
	            private static final String FORMAT = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

	            @Override
	            public synchronized String format(LogRecord lr) {
	                return String.format(FORMAT,
	                        new Date(lr.getMillis()),
	                        lr.getLevel().getLocalizedName(),
	                        lr.getMessage()
	                );
	            }
	        });
			
			LOG.addHandler(fileHandler);

			// Setting levels to handlers and LOGGER
			fileHandler.setLevel(logLevel);
			LOG.setLevel(logLevel);

		} catch (IOException e) {
			LOG.log(Level.SEVERE, "Error occured in FileHandler.", e);
		}
	}

	/**
	 * Create the logging directory structure
	 * 
	 * @param logDir
	 * @return
	 */
	private static String initLogDirs(String logDir) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatYearMonth = DateTimeFormatter.ofPattern("yyyy-MM");
		DateTimeFormatter formatDay = DateTimeFormatter.ofPattern("dd");
		
		// Create main logging Directory
		logDir = logDir + File.separator + "logs";
		File directory = new File(logDir);
		if (!directory.exists()) {
			LOG.log(Level.INFO, "Creating Log directory: " + directory.toString());
			directory.mkdir();
		}

		// Create monthly logging Directory
		logDir = logDir + File.separator + formatYearMonth.format(now);
		directory = new File(logDir);
		if (!directory.exists()) {
			LOG.log(Level.INFO, "Creating Log directory: " + directory.toString());
			directory.mkdir();
		}
		
		// Generate current logFile
		String logFile = logDir + File.separator + formatDay.format(now) + ".log";
		LOG.log(Level.INFO, "LogFile: " + logFile);
		
		return logFile;
	}
}