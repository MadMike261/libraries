package com.madmike261.neworders;

public class NewSellOrder {
	private String tradePair;
	private double sellPrice;
	private double sellAmount;
	
	public String getTradePair() {
		return tradePair;
	}
	
	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public double getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(double sellAmount) {
		this.sellAmount = sellAmount;
	}
}