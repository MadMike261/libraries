package com.madmike261.neworders;

public class NewLinkedOrder {
	private NewBuyOrder newBuyOrder = new NewBuyOrder();
	private NewSellOrder newSellOrder = new NewSellOrder();
	
	public NewBuyOrder getNewBuyOrder() {
		return newBuyOrder;
	}
	
	public void setNewBuyOrder(NewBuyOrder newBuyOrder) {
		this.newBuyOrder = newBuyOrder;
	}

	public NewSellOrder getNewSellOrder() {
		return newSellOrder;
	}

	public void setNewSellOrder(NewSellOrder newSellOrder) {
		this.newSellOrder = newSellOrder;
	}
}