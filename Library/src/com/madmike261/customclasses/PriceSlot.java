package com.madmike261.customclasses;

import java.math.BigDecimal;
import java.util.ArrayList;

public class PriceSlot {
	private BigDecimal price;
	private int slotCount = 0;
	private ArrayList<String> orderIds = new ArrayList<String>();
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public int getSlotCount() {
		return slotCount;
	}
	
	public void setSlotCount(int slotCount) {
		this.slotCount = slotCount;
	}
	
	public PriceSlot(BigDecimal price) {
		this.price = price;
	}

	public ArrayList<String> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(ArrayList<String> orderIds) {
		this.orderIds = orderIds;
	}
}