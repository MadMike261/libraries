package com.madmike261.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.dto.marketdata.Trades.TradeSortType;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.madmike261.customclasses.BotInstance;
import com.madmike261.customclasses.BotSettings;
import com.madmike261.customclasses.PriceSlots;
import com.madmike261.customclasses.TradePair;
import com.madmike261.customclasses.customUserTrades;
import com.madmike261.runstate.ClosedOrderList;

public class JSON {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");
	
	public static ClosedOrderList getStoredBuyOrders(String targetFile) {
		ClosedOrderList storedBuyOrders = new ClosedOrderList();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		try (FileReader file = new FileReader(targetFile)) {
			storedBuyOrders = gson.fromJson(file, ClosedOrderList.class);
		}
		catch (Exception e) {
			LOG.log(Level.INFO, "Something went wrong while reading stored buy orders: " + targetFile);
			LOG.log(Level.INFO, "Creating File with default settings.");
			createStoredBuyOrders(targetFile, storedBuyOrders);
		}
		
		return storedBuyOrders;
	}

	public static void createStoredBuyOrders(String targetFile, ClosedOrderList storedBuyOrders) {
		File settingsFile = new File(targetFile);
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		String json = gson.toJson(storedBuyOrders);
		
		try (FileWriter file = new FileWriter(settingsFile, false)) {
			file.write(json);
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when creating stored buy orders: " + targetFile);
		}
	}
	
	public static void createBotSettingsFile(String targetFile, BotSettings botSettings) {
		File settingsFile = new File(targetFile);
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		String json = gson.toJson(botSettings);
		
		LOG.log(Level.INFO, json);
		
		try {
			Files.deleteIfExists(settingsFile.toPath());
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when deleting old BotSettingsFile: " + targetFile);
		}
		
		try (FileWriter file = new FileWriter(settingsFile, false)) {
			file.write(json);
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when creating BotSettingsFile: " + targetFile);
		}
	}
	
	public static BotSettings getBotSettings(String targetFile) {
		BotSettings botSettings = new BotSettings();
		BotInstance botInstance = new BotInstance();
		TradePair tradePairs = new TradePair();
		botInstance.getTradePairs().add(tradePairs);
		botSettings.getBotSettings().add(botInstance);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		try (FileReader file = new FileReader(targetFile)) {
			botSettings = gson.fromJson(file, BotSettings.class);
		}
		catch (Exception e) {
			LOG.log(Level.INFO, "Something went wrong while reading the BotSettingsFile: " + targetFile);
			LOG.log(Level.INFO, "Creating BotSettingsFile with default settings.");
			createBotSettingsFile(targetFile, botSettings);
		}
		
		return botSettings;
	}
	
	public static void createPriceSlotsFile(String targetFile, PriceSlots priceSlots) {
		File settingsFile = new File(targetFile);
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		String json = gson.toJson(priceSlots);
		
		try {
			Files.deleteIfExists(settingsFile.toPath());
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when deleting old PriceSlotsFile: " + targetFile);
		}
		
		try (FileWriter file = new FileWriter(settingsFile, false)) {
			file.write(json);
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when creating PriceSlotsFile: " + targetFile);
		}
	}
	
	public static PriceSlots getPriceSlots(String targetFile) {
		PriceSlots priceSlots = new PriceSlots();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		try (FileReader file = new FileReader(targetFile)) {
			priceSlots = gson.fromJson(file, PriceSlots.class);
		}
		catch (Exception e) {
			LOG.log(Level.INFO, "Something went wrong while reading the PriceSlotsFile: " + targetFile);
			LOG.log(Level.INFO, "Creating PriceSlotsFile with default settings.");
			createPriceSlotsFile(targetFile, priceSlots);
		}
		
		return priceSlots;
	}
	
	public static void createProcessedClosedOrdersFile(String targetFile, UserTrades closedOrders) {
		File settingsFile = new File(targetFile);
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		
		String json = gson.toJson(closedOrders);
		
		try {
			Files.deleteIfExists(settingsFile.toPath());
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when deleting old ClosedOrdersFile: " + targetFile);
			LOG.log(Level.INFO, e.toString());
		}
		
		try (FileWriter file = new FileWriter(settingsFile, false)) {
			file.write(json);
		}
		catch(Exception e) {
			LOG.log(Level.INFO, "Something went wrong when creating ClosedOrdersFile: " + targetFile);
		}
	}
	
	public static UserTrades getProcessedClosedOrders(String targetFile) {
		/*
		OrderType type,
	      BigDecimal originalAmount,
	      CurrencyPair currencyPair,
	      BigDecimal price,
	      Date timestamp,
	      String id,
	      String orderId,
	      BigDecimal feeAmount,
	      Currency feeCurrency
	    */
		
		List<UserTrade> tempList = new ArrayList<>();
		UserTrades closedOrders = new UserTrades(tempList, TradeSortType.SortByID);
		
		Gson gson = new GsonBuilder().create();
		
		try (FileReader file = new FileReader(targetFile)) {
			customUserTrades tempTrade = gson.fromJson(file, customUserTrades.class);
			
			for(UserTrade currentTrade : tempTrade.getTrades()) {
				closedOrders.getUserTrades().add(currentTrade);
			}
			
			return closedOrders;
		}
		catch (Exception e) {
			LOG.log(Level.INFO, "Something went wrong while reading the ProcessedClosedOrdersFile: " + targetFile);
			File tempFile = new File(targetFile);
			if(tempFile.exists() && tempFile.isFile()) {
				LOG.log(Level.SEVERE, "File Exists, Reading error, not creating new file.");
			}
			else {
				LOG.log(Level.INFO, "Creating ProcessedClosedOrdersFile with default settings.");
				createProcessedClosedOrdersFile(targetFile, closedOrders);
			}
		}
		
		return closedOrders;
	}
}