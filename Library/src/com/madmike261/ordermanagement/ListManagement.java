package com.madmike261.ordermanagement;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;

import com.madmike261.customclasses.TradePair;
import com.madmike261.customclasses.customUserTrades;

public class ListManagement {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");
	
	public static UserTrades removeProcessedOrders(UserTrades processingList, UserTrades processedOrders) {
		customUserTrades foundList = new customUserTrades();
		
		LOG.log(Level.FINE, "processingList size: " + processingList.getUserTrades().size());
		LOG.log(Level.FINE, "processedOrders size: " + processedOrders.getUserTrades().size());
		
		if((!processingList.getUserTrades().isEmpty()) && (!processedOrders.getUserTrades().isEmpty())) {
			LOG.log(Level.FINE, "Starting list comparison.");
			for(int i = 0; i < (processingList.getUserTrades().size()); i++) {
				UserTrade newTrade = processingList.getUserTrades().get(i);
				boolean found = false;
				for(UserTrade storedTrade : processedOrders.getUserTrades()) {
					if(new BigDecimal(newTrade.getOrderId()).compareTo(new BigDecimal(storedTrade.getOrderId())) == 0) {
						found = true;
					}
				}

				if(found) {
					foundList.getTrades().add(newTrade);
				}
				else {
					LOG.log(Level.INFO, "Order not found, processing: " + newTrade.toString());
				}
			}
		}
		
		return removeFoundOrders(processingList, foundList);
	}
	
	private static UserTrades removeFoundOrders(UserTrades processingList, customUserTrades foundList) {
		if(!foundList.getTrades().isEmpty()) {
			for(UserTrade currentTrade : foundList.getTrades()) {
				processingList.getUserTrades().remove(currentTrade);
			}
		}
		return processingList;
	}
	
	public static UserTrades processPartialBuys(UserTrades ordersToProcess, TradePair tradePair) {
		//find partial trades
		customUserTrades partialTrades = new customUserTrades();
		for(UserTrade currentTrade : ordersToProcess.getUserTrades()) {
			if(currentTrade.getOriginalAmount().compareTo(tradePair.getOrderSize()) == -1 ) {
				partialTrades.getTrades().add(currentTrade);
			}
		}
		LOG.log(Level.FINE, "Partial orders: " + String.valueOf(partialTrades.getTrades().size()));
		
		//combine partial trades
		customUserTrades combinedTrades = new customUserTrades();
		customUserTrades removeTrades = new customUserTrades();
		for(UserTrade currentTrade : partialTrades.getTrades()) {
			for(UserTrade currentTrade2 : partialTrades.getTrades()) {
				if(currentTrade.getPrice().equals(currentTrade2.getPrice())) {
					if(currentTrade.getId().equals(currentTrade2.getId())) {
						LOG.log(Level.FINE, "Same orderID: " + currentTrade.getId() + " & " + currentTrade2.getId());
					}
					else if(removeTrades.getTrades().contains(currentTrade)) {
						LOG.log(Level.FINE, "Already combined:"  + currentTrade.getId() + " with " + currentTrade2.getId());
					}
					else {
						LOG.log(Level.FINE, "Combining: " + currentTrade.getId() + " with " + currentTrade2.getId());
						UserTrade tempTrade = currentTrade;
						tempTrade.setOriginalAmount(currentTrade.getOriginalAmount().add(currentTrade2.getOriginalAmount()));
						combinedTrades.getTrades().add(tempTrade);
						removeTrades.getTrades().add(currentTrade);
						removeTrades.getTrades().add(currentTrade2);
					}
				}
			}
		}
		
		if(removeTrades.getTrades().size() > 0) {
			for(UserTrade currentTrade : removeTrades.getTrades()) {
				ordersToProcess.getTrades().remove(currentTrade);
			}
		}
		
		if(combinedTrades.getTrades().size() > 0) {
			for(UserTrade currentTrade : combinedTrades.getTrades()) {
				ordersToProcess.getTrades().add(currentTrade);
			}
		}
		
		return ordersToProcess;
	}
}