package com.madmike261.runstate;

import java.util.ArrayList;
import java.util.List;

public class PriceList {
	private List<PricePair> priceList = new ArrayList<>();
	
	public PricePair getPricePair(String pricePair) {
		PricePair returnPair = new PricePair("Dummy");
		for(PricePair currentPair : getPriceList()) {
			if((pricePair).equals(currentPair.getTradePair())) {
				returnPair = currentPair;
			}
		}
		return returnPair;
	}
	
	public void setPricePair(PricePair pricePair) {
		for(PricePair currentPair : getPriceList()) {
			if((pricePair.getTradePair()).equals(currentPair.getTradePair())) {
				int replaceIndex = getPriceList().indexOf(currentPair);
				getPriceList().set(replaceIndex, pricePair);
			}
		}
	}

	public List<PricePair> getPriceList() {
		return this.priceList;
	}
	
	public void setPriceList(List<PricePair> newList) {
		this.priceList = newList;
	}
	
	
	public PriceList() {
		PricePair BTCUSD = new PricePair("BTC_USD");
		priceList.add(BTCUSD);
		
		PricePair ETHUSD = new PricePair("ETH_USD");
		priceList.add(ETHUSD);
		
		PricePair XRPUSD = new PricePair("XRP_USD");
		priceList.add(XRPUSD);
	}
}