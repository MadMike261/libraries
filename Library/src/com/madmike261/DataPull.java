package com.madmike261;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trades.TradeSortType;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.dto.trade.OpenOrders;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;
import org.knowm.xchange.service.account.AccountService;
import org.knowm.xchange.service.trade.params.TradeHistoryParamCurrencyPair;
import org.knowm.xchange.service.trade.params.orders.OpenOrdersParamCurrencyPair;

import com.madmike261.runstate.RunState;
import com.madmike261.util.TimeUtil;

public class DataPull {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");

	//TODO add error handling for this function
	public static void updatePrices(RunState runState, CurrencyPair currencyPair) {
		try {
			Ticker ticker = runState.getExchange().getMarketDataService().getTicker(currencyPair);
			runState.setTicker(ticker);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.toString());
		}
		TimeUtil.sleep(100);
	}

	public static void updateOpenOrders(RunState runState, CurrencyPair currencyPair) {
		try {
			OpenOrdersParamCurrencyPair openOrdersParamCurrencyPair = (OpenOrdersParamCurrencyPair) runState.getExchange().getTradeService().createOpenOrdersParams();
			openOrdersParamCurrencyPair.setCurrencyPair(currencyPair);
			
			OpenOrders returnedOrders = runState.getExchange().getTradeService().getOpenOrders(openOrdersParamCurrencyPair);
			
			splitOpenOrders(runState, returnedOrders);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.toString());
		}
		TimeUtil.sleep(100);
	}

	private static void splitOpenOrders(RunState runState, OpenOrders returnedOrders) {
		List<LimitOrder> emptyList1 = new ArrayList<>();
		List<LimitOrder> emptyList2 = new ArrayList<>();
		OpenOrders openBuyOrders = new OpenOrders(emptyList1);
		OpenOrders openSellOrders = new OpenOrders(emptyList2);
		
		if(!returnedOrders.getOpenOrders().isEmpty()) {
			for(LimitOrder currentOrder : returnedOrders.getOpenOrders()) {
				if(currentOrder.getType() == OrderType.BID) {
					openBuyOrders.getOpenOrders().add(currentOrder);
				}
				
				if(currentOrder.getType() == OrderType.ASK) {
					openSellOrders.getOpenOrders().add(currentOrder);
				}
			}
		}
		
		runState.setOpenBuyOrders(openBuyOrders);
		runState.setOpenSellOrders(openSellOrders);
	}

	public static void updateClosedOrders(RunState runState, CurrencyPair currencyPair) {
		try {
			TradeHistoryParamCurrencyPair tradeHistoryParamCurrencyPair = (TradeHistoryParamCurrencyPair) runState.getExchange().getTradeService().createTradeHistoryParams();
			tradeHistoryParamCurrencyPair.setCurrencyPair(currencyPair);
			
			UserTrades returnedTrades = runState.getExchange().getTradeService().getTradeHistory(tradeHistoryParamCurrencyPair);
			
			splitClosedOrders(runState, returnedTrades);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.toString());
		}
		TimeUtil.sleep(100);
	}

	private static void splitClosedOrders(RunState runState, UserTrades returnedTrades) {
		List<UserTrade> tempList = new ArrayList<>();
		UserTrades closedBuyOrders = new UserTrades(tempList, TradeSortType.SortByID);
		UserTrades closedSellOrders = new UserTrades(tempList, TradeSortType.SortByID);
		
		if(!returnedTrades.getUserTrades().isEmpty()) {
			for(UserTrade currentTrade : returnedTrades.getUserTrades()) {
				if(currentTrade.getType() == OrderType.BID) {
					closedBuyOrders.getUserTrades().add(currentTrade);
				}
				
				if(currentTrade.getType() == OrderType.ASK) {
					closedSellOrders.getUserTrades().add(currentTrade);
				}
			}
		}
		runState.setClosedBuyOrders(closedBuyOrders);
		runState.setClosedSellOrders(closedSellOrders);
	}

	public static void updateBalance(RunState runState) {
		AccountService accountService = runState.getExchange().getAccountService();

		// Get the account information
		AccountInfo accountInfo;
		try {
			accountInfo = accountService.getAccountInfo();
			if(runState.getExchangeName().equalsIgnoreCase("Bitfinex")) {
				runState.setWallet(accountInfo.getWallet("exchange"));
			}
			else {
				runState.setWallet(accountInfo.getWallet());
			}
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
		TimeUtil.sleep(100);
	}

}