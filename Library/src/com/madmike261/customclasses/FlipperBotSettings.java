package com.madmike261.customclasses;

import java.util.ArrayList;
import java.util.List;

public class FlipperBotSettings {
	private List<FlipperBotInstance> flipperBotSettings = new ArrayList<>();

	public List<FlipperBotInstance> getFlipperBotSettings() {
		return flipperBotSettings;
	}

	public void setFlipperBotSettings(List<FlipperBotInstance> flipperBotSettings) {
		this.flipperBotSettings = flipperBotSettings;
	}
	
	public FlipperBotInstance getFlipperBotInstance(String tradePair) {
		FlipperBotInstance returnInstance = new FlipperBotInstance();
		
		for(FlipperBotInstance currentInstance : getFlipperBotSettings()) {
			if(currentInstance.getTradePair().equals(tradePair)) {
				returnInstance = currentInstance;
			}
		}
		
		return returnInstance;
	}
}