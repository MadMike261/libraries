package com.madmike261.neworders;

public class NewBuyOrder {
	private String tradePair;
	private double buyPrice;
	private double buyAmount;
	
	public String getTradePair() {
		return tradePair;
	}
	
	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public double getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(double buyPrice) {
		this.buyPrice = buyPrice;
	}

	public double getBuyAmount() {
		return buyAmount;
	}

	public void setBuyAmount(double buyAmount) {
		this.buyAmount = buyAmount;
	}
}