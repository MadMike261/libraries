package com.madmike261.runstate;

public class ClosedOrder {
	private String orderID;
	private String tradePair;
	private double volume;
	private double price;
	private int closetm;
	
	public String getOrderID() {
		return orderID;
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getTradePair() {
		return tradePair;
	}

	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getClosetm() {
		return closetm;
	}

	public void setClosetm(int closeTM) {
		this.closetm = closeTM;
	}
}