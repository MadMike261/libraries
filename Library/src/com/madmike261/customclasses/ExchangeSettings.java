package com.madmike261.customclasses;

public class ExchangeSettings {
	private String publicKey;
	private String privateKey;
	private String exchange;
	private int ratelimit = 3000;
	private double exchangeFee = 0.5;
	
	public String getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public int getRatelimit() {
		return ratelimit;
	}

	public void setRatelimit(int ratelimit) {
		this.ratelimit = ratelimit;
	}

	public double getExchangeFee() {
		return exchangeFee;
	}

	public void setExchangeFee(double exchangeFee) {
		this.exchangeFee = exchangeFee;
	}
	
}