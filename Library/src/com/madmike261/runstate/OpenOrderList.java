package com.madmike261.runstate;

import java.util.ArrayList;
import java.util.List;

public class OpenOrderList {
	private List<OpenOrder> openOrderList = new ArrayList<>();

	public List<OpenOrder> getOpenOrderList() {
		return openOrderList;
	}

	public void setOpenOrderList(List<OpenOrder> openOrderList) {
		this.openOrderList = openOrderList;
	}
}