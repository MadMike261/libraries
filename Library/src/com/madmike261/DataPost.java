package com.madmike261;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.bitfinex.v1.dto.trade.BitfinexOrderFlags;
import org.knowm.xchange.dto.Order.OrderType;
import org.knowm.xchange.dto.trade.LimitOrder;

import com.madmike261.customclasses.TradePair;
import com.madmike261.runstate.RunState;
import com.madmike261.util.TimeUtil;

public class DataPost {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");

	public static void cancelOpenOrder(RunState runState, String orderID) {
		LOG.log(Level.INFO, "Cancelling order: " + orderID);
		
		try {
			runState.getExchange().getTradeService().cancelOrder(orderID);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.toString());
		}
		
		TimeUtil.sleep(100);
	}

	public static void placeNewLimitBuyOrder(RunState runState, TradePair tradePair, BigDecimal targetPrice) {
		LimitOrder limitOrder = new LimitOrder((OrderType.BID), tradePair.getOrderSize(), tradePair.getCurrencyPair(), null, null, targetPrice);
		limitOrder.addOrderFlag(BitfinexOrderFlags.POST_ONLY);
	    
		if(targetPrice.compareTo(tradePair.getUpperBoundary()) == 1) {
			LOG.log(Level.INFO, "targetPrice: " + targetPrice + " above upperBoundary: " + tradePair.getUpperBoundary());
			return;
		}
		
		if(targetPrice.compareTo(tradePair.getLowerBoundary()) == -1) {
			LOG.log(Level.INFO, "targetPrice: " + targetPrice + " below lowerBoundary: " + tradePair.getLowerBoundary());
			return;
		}
		
		try {
			LOG.log(Level.INFO, "Posting limit buy order: " + limitOrder.toString());
			String limitOrderReturnValue = runState.getExchange().getTradeService().placeLimitOrder(limitOrder);
			LOG.log(Level.INFO, "Limit Order return value: " + limitOrderReturnValue);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.toString());
		}
		
		TimeUtil.sleep(100);
	}

	public static String placeNewLimitSellOrder(RunState runState, TradePair tradePair, BigDecimal targetPrice, BigDecimal targetSize) {
		LimitOrder limitOrder = new LimitOrder((OrderType.ASK), targetSize, tradePair.getCurrencyPair(), null, null, targetPrice);
		String limitOrderReturnValue = "9999";
	    
		try {
			LOG.log(Level.INFO, "Posting limit sell order: " + limitOrder.toString());
			limitOrderReturnValue = runState.getExchange().getTradeService().placeLimitOrder(limitOrder);
			LOG.log(Level.INFO, "Limit Order return value: " + limitOrderReturnValue);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.toString());
		}
		TimeUtil.sleep(100);
		
		return limitOrderReturnValue;
	}
}