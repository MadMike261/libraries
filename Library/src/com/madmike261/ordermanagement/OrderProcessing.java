package com.madmike261.ordermanagement;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;

import com.madmike261.DataPost;
import com.madmike261.customclasses.TradePair;
import com.madmike261.runstate.RunState;
import com.madmike261.util.SlotManagement;

public class OrderProcessing {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");
	
	public static void processClosedSells(UserTrades ordersToProcess, RunState runState) {
		if(!ordersToProcess.getUserTrades().isEmpty()) {
			LOG.log(Level.FINE, "processing unprocessed sells: " + ordersToProcess.getUserTrades().size());
			for(UserTrade currentTrade : ordersToProcess.getUserTrades()) {
				SlotManagement.decreaseSlot(currentTrade, runState);
			}
		}
	}
	
	public static void processClosedBuys(UserTrades ordersToProcess, RunState runState, TradePair tradePair) {
		for(UserTrade currentTrade : ordersToProcess.getUserTrades()) {
			//generate sell price
			BigDecimal targetPrice = currentTrade.getPrice().divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(100 + runState.getExchangeFee() + runState.getProfitPercentage()));
			targetPrice = targetPrice.setScale(currentTrade.getPrice().scale(), BigDecimal.ROUND_UP);

			//generate position size
			BigDecimal targetSize = currentTrade.getOriginalAmount().divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(100 - runState.getProfitPercentage() + ((runState.getProfitPercentage()/100)* runState.getTakeProfitPercentage())));
			targetSize = targetSize.setScale(8, BigDecimal.ROUND_UP).stripTrailingZeros();

			//place sell order
			String orderId = DataPost.placeNewLimitSellOrder(runState, tradePair, targetPrice, targetSize);
			SlotManagement.increaseSlot(currentTrade, runState, orderId);
		}
	}
}