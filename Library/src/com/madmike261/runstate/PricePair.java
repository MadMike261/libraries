package com.madmike261.runstate;

import java.math.BigDecimal;

public class PricePair {
	private String tradePair;
	private BigDecimal tradePrice;
	private BigDecimal nextBuy;
	private BigDecimal nextSell;
	private double Low24h;
	private double High24h;
	private int updateTime;
	
	public PricePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public String getTradePair() {
		return tradePair;
	}
	
	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public BigDecimal getTradePrice() {
		return tradePrice;
	}

	public void setTradePrice(BigDecimal bigDecimal) {
		this.tradePrice = bigDecimal;
	}

	public BigDecimal getNextBuy() {
		return nextBuy;
	}

	public void setNextBuy(BigDecimal bigDecimal) {
		this.nextBuy = bigDecimal;
	}

	public BigDecimal getNextSell() {
		return nextSell;
	}

	public void setNextSell(BigDecimal bigDecimal) {
		this.nextSell = bigDecimal;
	}

	public double getLow24h() {
		return Low24h;
	}

	public void setLow24h(double low24h) {
		Low24h = low24h;
	}

	public double getHigh24h() {
		return High24h;
	}

	public void setHigh24h(double high24h) {
		High24h = high24h;
	}

	public int getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(int updateTime) {
		this.updateTime = updateTime;
	}
}