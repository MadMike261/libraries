package com.madmike261.customclasses;

import java.util.ArrayList;
import java.util.List;

import org.knowm.xchange.dto.trade.UserTrade;

public class customUserTrades {
	private List<UserTrade> trades;
	
	public customUserTrades() {
		this.trades = new ArrayList<UserTrade>();
	}

	public List<UserTrade> getTrades() {
		return trades;
	}

	public void setTrades(List<UserTrade> trades) {
		this.trades = trades;
	}
	
	
}