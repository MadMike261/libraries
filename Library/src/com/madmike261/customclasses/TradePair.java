package com.madmike261.customclasses;

import java.math.BigDecimal;

import org.knowm.xchange.currency.CurrencyPair;

public class TradePair {
	private String tradePair = "example ETHEUR";
	private String tradeType = "COINPAIR or FIATPAIR";
	private String baseCurrency = "example EUR";
	private BigDecimal upperBoundary;
	private BigDecimal lowerBoundary;
	private BigDecimal startTradingBalance = new BigDecimal(1000);
	private BigDecimal orderSize = new BigDecimal(0.01);
	private int minimumPriceScale;
	private int maxOpenBuyOrders = 1;
	private int maxSlotCount;
	
	public BigDecimal getOrderSize() {
		return orderSize;
	}

	public void setOrderSize(BigDecimal orderSize) {
		this.orderSize = orderSize;
	}

	public String getTradePair() {
		return tradePair;
	}

	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public BigDecimal getStartTradingBalance() {
		return startTradingBalance;
	}

	public void setStartTradingBalance(BigDecimal startTradingBalance) {
		this.startTradingBalance = startTradingBalance;
	}

	public CurrencyPair getCurrencyPair() {
		String[] splitString = tradePair.split("_");
		CurrencyPair currencyPair = new CurrencyPair(splitString[0],splitString[1]);
		return currencyPair;
	}

	public BigDecimal getUpperBoundary() {
		return upperBoundary;
	}

	public void setUpperBoundary(BigDecimal upperBoundary) {
		this.upperBoundary = upperBoundary;
	}

	public BigDecimal getLowerBoundary() {
		return lowerBoundary;
	}

	public void setLowerBoundary(BigDecimal lowerBoundary) {
		this.lowerBoundary = lowerBoundary;
	}

	public int getMaxOpenBuyOrders() {
		return maxOpenBuyOrders;
	}

	public void setMaxOpenBuyOrders(int maxOpenBuyOrders) {
		this.maxOpenBuyOrders = maxOpenBuyOrders;
	}

	public int getMaxSlotCount() {
		return maxSlotCount;
	}

	public void setMaxSlotCount(int maxSlotCount) {
		this.maxSlotCount = maxSlotCount;
	}

	public int getMinimumPriceScale() {
		return minimumPriceScale;
	}

	public void setMinimumPriceScale(int minimumPriceScale) {
		this.minimumPriceScale = minimumPriceScale;
	}
}