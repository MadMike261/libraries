package com.madmike261.runstate;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bitfinex.BitfinexExchange;
import org.knowm.xchange.bitstamp.BitstampExchange;
import org.knowm.xchange.dto.account.Wallet;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.trade.OpenOrders;
import org.knowm.xchange.dto.trade.UserTrades;

import com.madmike261.DataPull;
import com.madmike261.customclasses.BotInstance;
import com.madmike261.customclasses.TradePair;

public class RunState {
	private static Exchange exchange;
	private String exchangeName;
	private Ticker ticker;
	private OpenOrders openBuyOrders;
	private OpenOrders openSellOrders;
	private UserTrades closedBuyOrders;
	private UserTrades closedSellOrders;
	private Wallet wallet;
	private String runningDir = findRunningDir();
	private String botName;
	private double exchangeFee;
	private double offsetPercentage;
	private double profitPercentage;
	private double takeProfitPercentage;
	
	public void init(BotInstance currentInstance) {
		this.exchangeFee = currentInstance.getExchangeFee();
		this.offsetPercentage = currentInstance.getOffsetPercentage();
		this.profitPercentage = currentInstance.getProfitPercentage();
		this.takeProfitPercentage = currentInstance.getTakeProfitPercentage();
		this.exchangeName = currentInstance.getExchange();
		
		this.setBotName(currentInstance.getBotName());
		this.generateExchange(currentInstance);
		this.updateWallet();
	}
	
	public void update(TradePair tradePair) {
		DataPull.updatePrices(this, tradePair.getCurrencyPair());
		DataPull.updateOpenOrders(this, tradePair.getCurrencyPair());
		DataPull.updateClosedOrders(this, tradePair.getCurrencyPair());
	}
	
	public void updateWallet() {
		DataPull.updateBalance(this);
	}
	
	public Exchange getExchange() {
		return RunState.exchange;
	}

	public void setExchange(Exchange newExchange) {
		RunState.exchange = newExchange;
	}
	
	public void generateExchange(BotInstance botInstance) {
		//Bitstamp
		if(botInstance.getExchange().equalsIgnoreCase("Bitstamp")) {
			ExchangeSpecification Bitstamp = new BitstampExchange().getDefaultExchangeSpecification();
			Bitstamp.setUserName(botInstance.getUserName());
			Bitstamp.setApiKey(botInstance.getPublicKey());
			Bitstamp.setSecretKey(botInstance.getPrivateKey());
			
			exchange = ExchangeFactory.INSTANCE.createExchange(Bitstamp);
		}
		
		//Bitfinex
		if(botInstance.getExchange().equalsIgnoreCase("Bitfinex")) {
			Exchange bfx = ExchangeFactory.INSTANCE.createExchange(BitfinexExchange.class.getName());
			ExchangeSpecification Bitfinex = new BitfinexExchange().getDefaultExchangeSpecification();
			Bitfinex.setApiKey(botInstance.getPublicKey());
			Bitfinex.setSecretKey(botInstance.getPrivateKey());
			//Bitfinex.setShouldLoadRemoteMetaData(false); doesnt seem to work, changed default
			bfx.applySpecification(Bitfinex);
			
		    exchange = bfx;
		}
	}
	
	public OpenOrders getOpenBuyOrders() {
		return openBuyOrders;
	}
	
	public void setOpenBuyOrders(OpenOrders openBuyOrderList) {
		this.openBuyOrders= openBuyOrderList;
	}
	
	public Wallet getWallet() {
		return wallet;
	}
	
	public void setWallet(Wallet newWallet) {
		wallet = newWallet;
	}

	public String getRunningDir() {
		return runningDir;
	}

	public void setRunningDir(String runningDir) {
		this.runningDir = runningDir;
	}

	public OpenOrders getOpenSellOrders() {
		return openSellOrders;
	}

	public void setOpenSellOrders(OpenOrders openSellOrders) {
		this.openSellOrders = openSellOrders;
	}

	public double getExchangeFee() {
		return exchangeFee;
	}

	public void setExchangeFee(double exchangeFree) {
		this.exchangeFee = exchangeFree;
	}

	public Ticker getTicker() {
		return ticker;
	}

	public void setTicker(Ticker ticker) {
		this.ticker = ticker;
	}

	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public double getOffsetPercentage() {
		return offsetPercentage;
	}

	public void setOffsetPercentage(double offsetPercentage) {
		this.offsetPercentage = offsetPercentage;
	}

	public double getProfitPercentage() {
		return profitPercentage;
	}

	public void setProfitPercentage(double profitPercentage) {
		this.profitPercentage = profitPercentage;
	}

	public double getTakeProfitPercentage() {
		return takeProfitPercentage;
	}

	public void setTakeProfitPercentage(double takeProfitPercentage) {
		this.takeProfitPercentage = takeProfitPercentage;
	}

	public UserTrades getClosedBuyOrders() {
		return closedBuyOrders;
	}

	public void setClosedBuyOrders(UserTrades closedBuyOrders) {
		this.closedBuyOrders = closedBuyOrders;
	}

	public UserTrades getClosedSellOrders() {
		return closedSellOrders;
	}

	public void setClosedSellOrders(UserTrades closedSellOrders) {
		this.closedSellOrders = closedSellOrders;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}
	
	public String getTickerPlainName() {
		String currencyPair = this.getTicker().getCurrencyPair().toString();
		currencyPair = currencyPair.replace("/", "");
		return currencyPair;
	}
	
	private String findRunningDir() {
		if(SystemUtils.IS_OS_WINDOWS) {
			return "C:" + File.separator + "MadBot" + File.separator;
		}
		else if(SystemUtils.IS_OS_LINUX) {
			return File.separator + "MadBot" + File.separator;
		}
		else {
			return SystemUtils.getUserDir().toString();
		}
	}
}