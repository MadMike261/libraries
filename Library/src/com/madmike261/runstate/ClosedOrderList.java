package com.madmike261.runstate;

import java.util.ArrayList;
import java.util.List;

public class ClosedOrderList {
	private List<ClosedOrder> closedOrderList = new ArrayList<>();
	private int lastUpdateTime = (int) (System.currentTimeMillis() / 1000l);

	public List<ClosedOrder> getClosedOrderList() {
		return closedOrderList;
	}

	public void setClosedOrderList(List<ClosedOrder> closedOrderList) {
		this.closedOrderList = closedOrderList;
	}

	public int getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(int lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
}