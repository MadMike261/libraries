package com.madmike261.customclasses;

import java.util.ArrayList;
import java.util.List;

public class BotSettings {
	private List<BotInstance> botSettings = new ArrayList<>();

	public List<BotInstance> getBotSettings() {
		return botSettings;
	}

	public void setBotSettings(List<BotInstance> botSettings) {
		this.botSettings = botSettings;
	}
	
}