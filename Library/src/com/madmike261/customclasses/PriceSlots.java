package com.madmike261.customclasses;

import java.util.ArrayList;
import java.util.List;

public class PriceSlots {
	private List<PriceSlot> priceSlots = new ArrayList<>();

	public List<PriceSlot> getPriceSlots() {
		return priceSlots;
	}

	public void setPriceSlots(List<PriceSlot> priceSlots) {
		this.priceSlots = priceSlots;
	}
}