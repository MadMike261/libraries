package com.madmike261.util;

import java.io.File;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.knowm.xchange.dto.trade.UserTrade;

import com.madmike261.customclasses.PriceSlot;
import com.madmike261.customclasses.PriceSlots;
import com.madmike261.customclasses.TradePair;
import com.madmike261.runstate.RunState;

public class SlotManagement {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");
	
	public static PriceSlots getSlots(RunState runState, TradePair tradePair) {
		PriceSlots newSlots = new PriceSlots();
		PriceSlots storedSlots = loadPriceSlots(runState);

		PriceSlots tempSlots = validateSlots(storedSlots, runState, tradePair);
		if(!(tempSlots.getPriceSlots().equals(storedSlots.getPriceSlots()))) {
			savePriceSlots(runState, tempSlots);
		}

		newSlots = getOrderSlots(runState.getTicker().getLast(), tempSlots, tradePair.getMaxOpenBuyOrders());
		return newSlots;
	}

	private static PriceSlots getOrderSlots(BigDecimal targetPrice, PriceSlots storedSlots, int maxOpenBuyOrders) {
		PriceSlots newSlots = new PriceSlots();
		int i = 0;

		while((storedSlots.getPriceSlots().get(i).getPrice().compareTo(targetPrice) == -1) && ((storedSlots.getPriceSlots().size() -1) > i)) {
			i++;
		}
		
		//decrease i by 1 to adjust for index starting at 0
		i--;
		for(int ii = 0; ii < maxOpenBuyOrders; ii++) {
			if(i >= 0) {
				newSlots.getPriceSlots().add(storedSlots.getPriceSlots().get(i));
			}
			i--;
		}

		return newSlots;
	}

	private static PriceSlots validateSlots(PriceSlots storedSlots, RunState runState, TradePair tradePair) {
		if(storedSlots.getPriceSlots().size() == 0) {
			storedSlots = generatePriceSlots(runState.getTicker().getLast(), runState.getOffsetPercentage(), tradePair);
		}

		if(storedSlots.getPriceSlots().get(0).getPrice().compareTo(runState.getTicker().getLast()) == 1) {
			storedSlots = addLowerSlots(runState.getTicker().getLast(), runState.getOffsetPercentage(),storedSlots);
		}

		if(storedSlots.getPriceSlots().get((storedSlots.getPriceSlots().size() - 1)).getPrice().compareTo(runState.getTicker().getLast()) == -1) {
			storedSlots = addHigherSlots(runState.getTicker().getLast(), runState.getOffsetPercentage(),storedSlots);
		}

		return storedSlots;
	}

	private static PriceSlots addLowerSlots(BigDecimal targetPrice, double offsetPercentage, PriceSlots storedSlots) {
		LOG.log(Level.INFO, "Adding lower slots");
		while(storedSlots.getPriceSlots().get(0).getPrice().compareTo(targetPrice) == 1) {
			BigDecimal newPrice;
			BigDecimal tempPrice = storedSlots.getPriceSlots().get(0).getPrice();
			newPrice = (tempPrice.divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(100 - offsetPercentage)));
			newPrice = newPrice.setScale(targetPrice.scale(), BigDecimal.ROUND_UP).stripTrailingZeros();
			storedSlots = addFirst(storedSlots, new PriceSlot(newPrice));
		}

		return storedSlots;
	}

	private static PriceSlots addHigherSlots(BigDecimal targetPrice, double offsetPercentage, PriceSlots storedSlots) {
		LOG.log(Level.INFO, "Adding higher slots");
		while(storedSlots.getPriceSlots().get((storedSlots.getPriceSlots().size() - 1)).getPrice().compareTo(targetPrice) == -1) {
			BigDecimal newPrice;
			BigDecimal tempPrice = storedSlots.getPriceSlots().get((storedSlots.getPriceSlots().size() - 1)).getPrice();
			newPrice = (tempPrice.divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(100 + offsetPercentage)));
			newPrice = newPrice.setScale(targetPrice.scale(), BigDecimal.ROUND_UP).stripTrailingZeros();
			storedSlots.getPriceSlots().add(new PriceSlot(newPrice));
		}

		return storedSlots;
	}

	private static PriceSlots generatePriceSlots(BigDecimal targetPrice, double offsetPercentage, TradePair tradePair) {
		PriceSlots newSlots = new PriceSlots();

		for(int i = 1; i < 11; i++) {
			BigDecimal newPrice;
			if(i==1) {
				newPrice = (targetPrice.divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(100 - offsetPercentage)));
			}
			else {
				BigDecimal tempPrice = newSlots.getPriceSlots().get(0).getPrice();
				newPrice = (tempPrice.divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(100 - offsetPercentage)));
			}
			int priceScale = tradePair.getMinimumPriceScale();
			if(priceScale < targetPrice.scale()) {
				priceScale = targetPrice.scale();
			}
			newPrice = newPrice.setScale(priceScale, BigDecimal.ROUND_UP).stripTrailingZeros();
			newSlots = addFirst(newSlots, new PriceSlot(newPrice));
		}

		return newSlots;
	}

	private static PriceSlots addFirst(PriceSlots priceSlots, PriceSlot priceSlot) {
		PriceSlots returnSlots = new PriceSlots();
		returnSlots.getPriceSlots().add(priceSlot);

		for(PriceSlot currentSlot : priceSlots.getPriceSlots()) {
			returnSlots.getPriceSlots().add(currentSlot);
		}

		return returnSlots;
	}

	public static void increaseSlot(UserTrade currentTrade, RunState runState, String orderId) {
		PriceSlots priceSlots = loadPriceSlots(runState);
		boolean matched = false;
		
		for(PriceSlot currentSlot : priceSlots.getPriceSlots()) {
			if(currentSlot.getPrice().compareTo(currentTrade.getPrice()) == 0) {
				currentSlot.setSlotCount((currentSlot.getSlotCount() + 1));
				currentSlot.getOrderIds().add(orderId);
				matched = true;
			}
		}
		
		if(matched == false) {
			for(int i = 0; i < (priceSlots.getPriceSlots().size() -2); i++) {
				if((priceSlots.getPriceSlots().get(i).getPrice().compareTo(currentTrade.getPrice()) == -1 ) && ((currentTrade.getPrice().compareTo(priceSlots.getPriceSlots().get(i+1).getPrice()) == -1 ))) {
					BigDecimal lowerDiff = currentTrade.getPrice().subtract(priceSlots.getPriceSlots().get(i).getPrice());
					BigDecimal higherDiff = priceSlots.getPriceSlots().get(i+1).getPrice().subtract(currentTrade.getPrice());
					
					if(lowerDiff.compareTo(higherDiff) == -1) {
						priceSlots.getPriceSlots().get(i).setSlotCount((priceSlots.getPriceSlots().get(i).getSlotCount() + 1));
						priceSlots.getPriceSlots().get(i).getOrderIds().add(orderId);
					}
					else {
						priceSlots.getPriceSlots().get(i+1).setSlotCount((priceSlots.getPriceSlots().get(i+1).getSlotCount() + 1));
						priceSlots.getPriceSlots().get(i+1).getOrderIds().add(orderId);
					}
					
					matched = true;
				}
			}
		}
		
		if(matched == false) {
			LOG.log(Level.INFO, "Increasing - Still no slot match..." + currentTrade.toString());
		}
		
		savePriceSlots(runState, priceSlots);
	}
	
	public static void decreaseSlot(UserTrade currentTrade, RunState runState) {
		PriceSlots priceSlots = loadPriceSlots(runState);
		boolean matched = false;
		
		
		for(PriceSlot currentSlot : priceSlots.getPriceSlots()) {
			if(currentSlot.getSlotCount() > 0) {
				for(String currentId : currentSlot.getOrderIds()) {
					if(new BigDecimal(currentId).compareTo(new BigDecimal(currentTrade.getOrderId())) == 0) {
						currentSlot.setSlotCount(currentSlot.getSlotCount() - 1);
						currentSlot.getOrderIds().remove(currentTrade.getId());
						matched = true;
					}
				}
			}
		}
		
		if(matched == false) {
			LOG.log(Level.INFO, "Decreasing - Still no slot match..." + currentTrade.toString());
		}
		
		savePriceSlots(runState, priceSlots);
	}
	
	private static PriceSlots loadPriceSlots(RunState runState) {
		String slotsFile = runState.getRunningDir() + runState.getBotName() + File.separator + runState.getTickerPlainName() + "_TradeSlots.json";
		PriceSlots returnSlots = JSON.getPriceSlots(slotsFile);
		
		return returnSlots;
	}
	
	private static void savePriceSlots(RunState runState, PriceSlots priceSlots) {
		String slotsFile = runState.getRunningDir() + runState.getBotName() + File.separator + runState.getTickerPlainName() + "_TradeSlots.json";
		JSON.createPriceSlotsFile(slotsFile, priceSlots);
	}
}