package com.madmike261.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TimeUtil {
	private static final Logger LOG = Logger.getLogger("MadBotLogger");
	
	public static void sleep(int sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (Exception e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}