package com.madmike261.runstate;

public class OpenOrder {
	private String orderID;
	private String tradePair;
	private double volume;
	private double volume_exec;
	private double price;
	
	public String getOrderID() {
		return orderID;
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getTradePair() {
		return tradePair;
	}

	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getVolume_exec() {
		return volume_exec;
	}

	public void setVolume_exec(double volume_exec) {
		this.volume_exec = volume_exec;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}