package com.madmike261.runstate;

import java.math.BigDecimal;

import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.dto.account.AccountInfo;

public class OldWallet {
	private BigDecimal EUR;
	private BigDecimal USD;
	private BigDecimal BTC;
	private BigDecimal ETH;
	private BigDecimal LTC;
	private BigDecimal XRP;
	private BigDecimal EOS;
	
	public BigDecimal getBalance(String currency) {
		BigDecimal returnValue = null;
		
		if(("EUR").equals(currency)) {
			returnValue = getEUR();
		}
		
		if(("USD").equals(currency)) {
			returnValue = getUSD();
		}
		
		if(("BTC").equals(currency)) {
			returnValue = getBTC();
		}
		
		if(("ETH").equals(currency)) {
			returnValue = getETH();
		}
		
		if(("LTC").equals(currency)) {
			returnValue = getLTC();
		}
		
		if(("XRP").equals(currency)) {
			returnValue = getXRP();
		}
		
		if(("EOS").equals(currency)) {
			returnValue = getEOS();
		}
		
		return returnValue;
	}
	
	public void setBalance(String currency, BigDecimal value) {
		if(("EUR").equals(currency)) {
			setEUR(value);
		}
		
		if(("USD").equals(currency)) {
			setUSD(value);
		}
		
		if(("BTC").equals(currency)) {
			setBTC(value);
		}
		
		if(("ETH").equals(currency)) {
			setETH(value);
		}
		
		if(("LTC").equals(currency)) {
			setLTC(value);
		}
		
		if(("XRP").equals(currency)) {
			setXRP(value);
		}
		
		if(("EOS").equals(currency)) {
			setEOS(value);
		}
	}
	
	public void updateBalance(AccountInfo accountInfo) {
		System.out.println(accountInfo.getWallet().toString());
		setEUR(accountInfo.getWallet().getBalance(Currency.getInstance("EUR")).getAvailable());
		setUSD(accountInfo.getWallet().getBalance(Currency.getInstance("USD")).getAvailable());
		setBTC(accountInfo.getWallet().getBalance(Currency.BTC).getAvailable());
		setETH(accountInfo.getWallet().getBalance(Currency.ETH).getAvailable());
		setLTC(accountInfo.getWallet().getBalance(Currency.LTC).getAvailable());
		setXRP(accountInfo.getWallet().getBalance(Currency.XRP).getAvailable());
		setEOS(accountInfo.getWallet().getBalance(Currency.EOS).getAvailable());
	}

	private BigDecimal getEUR() {
		return EUR;
	}

	private void setEUR(BigDecimal eUR) {
		EUR = eUR;
	}
	
	private BigDecimal getUSD() {
		return USD;
	}

	private void setUSD(BigDecimal uSD) {
		USD = uSD;
	}

	private BigDecimal getETH() {
		return ETH;
	}

	private void setETH(BigDecimal eTH) {
		ETH = eTH;
	}

	private BigDecimal getLTC() {
		return LTC;
	}

	private void setLTC(BigDecimal lTC) {
		LTC = lTC;
	}

	private BigDecimal getBTC() {
		return BTC;
	}

	private void setBTC(BigDecimal bTC) {
		BTC = bTC;
	}

	private BigDecimal getXRP() {
		return XRP;
	}

	private void setXRP(BigDecimal xRP) {
		XRP = xRP;
	}

	private BigDecimal getEOS() {
		return EOS;
	}

	private void setEOS(BigDecimal eOS) {
		EOS = eOS;
	}
}