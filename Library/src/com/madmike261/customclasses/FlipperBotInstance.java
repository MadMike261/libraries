package com.madmike261.customclasses;

public class FlipperBotInstance {
	private String tradePair = "example ETHEUR";
	private String tradeType = "COINPAIR or FIATPAIR";
	private String baseCurrency = "example EUR";
	private double startTradingBalance = 1000;
	private double orderSize = 0.01;
	private int maxOpenOrders = 1;
	private double startingPercentage = 0.1;
	private double offsetPercentage = 0.2;
	private double profitPercentage = 0.28;
	private double takeProfitPercentage = 100;
	
	public double getOrderSize() {
		return orderSize;
	}

	public void setOrderSize(double orderSize) {
		this.orderSize = orderSize;
	}

	public int getMaxOpenOrders() {
		return maxOpenOrders;
	}
	
	public void setMaxOpenOrders(int maxOpenOrders) {
		this.maxOpenOrders = maxOpenOrders;
	}

	public double getStartingPercentage() {
		return startingPercentage;
	}

	public void setStartingPercentage(double startingPercentage) {
		this.startingPercentage = startingPercentage;
	}

	public double getOffsetPercentage() {
		return offsetPercentage;
	}

	public void setOffsetPercentage(double offsetPercentage) {
		this.offsetPercentage = offsetPercentage;
	}

	public double getProfitPercentage() {
		return profitPercentage;
	}

	public void setProfitPercentage(double profitPercentage) {
		this.profitPercentage = profitPercentage;
	}

	public String getTradePair() {
		return tradePair;
	}

	public void setTradePair(String tradePair) {
		this.tradePair = tradePair;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public double getStartTradingBalance() {
		return startTradingBalance;
	}

	public void setStartTradingBalance(double startTradingBalance) {
		this.startTradingBalance = startTradingBalance;
	}

	public double getTakeProfitPercentage() {
		return takeProfitPercentage;
	}

	public void setTakeProfitPercentage(double takeProfitPercentage) {
		this.takeProfitPercentage = takeProfitPercentage;
	}
}